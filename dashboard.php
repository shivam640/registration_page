<?php 
    session_start();
    require_once('constant.php');
    require_once('connection.php');
    $sql = $conn-> query ("select email, fname, mname, lname, gender, address,
                        city, state, pincode, country, phone from user where
                        id=".$_SESSION['id']);
    if ($sql->num_rows > 0) {
        $data = $sql->fetch_array();
        if($data['gender'] == 1) {
            $data['gender'] = 'male';
        } else
            $data['gender'] = 'Female';
    }
    if (isset($_POST['submit'])) {
        $errors = array();
        $is_error=0;
        function isempty ($feild) {
            global $is_error;
            if(empty($feild)) {
                $is_error=1;
                return 'This feild can not be empty';
            }
        }
        function name_format ($expression, $feild) {
            global $is_error;
            if(!preg_match($expression, $feild)) {
                $is_error=1;
                return 'Only letters allowed!';
            }
        }
        $errors['name1'] = isempty($_POST['name']);
        $errors['name2'] = name_format(name_expression, $_POST['name']);
        $errors['mname1'] = name_format(name_expression, $_POST['mname']);
        $errors['lname1'] = isempty($_POST['lname']);
        $errors['lname2'] = name_format(name_expression, $_POST['lname']);
        $errors['gender'] = isempty($_POST['gender']);
        $errors['address'] = isempty($_POST['address']);
        $errors['city'] = isempty($_POST['city']);
        $errors['state'] = isempty($_POST['state']);
        $errors['pcode1'] = isempty($_POST['pcode']);
        if (!preg_match(pcode_expression,$_POST['pcode'])
             && !empty($_POST['pcode'])
         ) {
            $is_error=1;
            $errors['pcode2']= 'Must be 6 digits only'; 
        }
        $errors['country'] = isempty($_POST['country']);
        $errors['phoneno1'] = isempty($_POST['phoneno']);
        if (!preg_match(phone_expression,$_POST['phoneno'])
             && !empty($_POST['phoneno'])
         ) {
            $is_error=1;
            $errors['phoneno2']= 'Must be 10 digits only';
        }
        if ($is_error == 0) {
            $conn->query("update user set fname='".$_POST['name']."', mname='"
                    .$_POST['mname']."', lname='".$_POST['lname']."', gender="
                    .$_POST['gender'].", address='".$_POST['address']."',city='"
                    .$_POST['city']."', state='".$_POST['state']."', pincode="
                    .$_POST['pcode'].", country='".$_POST['country']."', phone="
                    .$_POST['phoneno']." where id=".$_SESSION['id']);
        }
    }
    if (isset($_POST['logout'])) {
        session_unset();
        session_destroy();
        header('Location: index.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My Page</a>
                </div>
                <div  class="collapse navbar-collapse" id="navcollapse">
                <form class="navbar-form navbar-right" action="dashboard.php"
                      method="post">
                    <div class="input-group">
                        <input type="text" class="form-control"
                               placeholder="Search">
                        <div class="input-group-btn">
                          <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                          </button>
                        </div>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li></li>
                    </ul>
                    <button type="submit" name="logout" class="btn"><span
                            class="glyphicon glyphicon-log-in"></span> Log Out
                    </button>
                </form>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-3" id="left">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a data-toggle="pill" href="#home">User Information
                            </a>
                        </li>
                        <li>
                            <a data-toggle="pill" href="#editpage">Edit Details
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9" id="right">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <p>
                                <h2>Basic Information</h2>
                                <h3>Name:</h3>
                                <?php echo $data['fname'].' '.$data['mname'].' '
                                .$data['lname']; 
                                ?>
                                <br>
                                <h3>Email Address:</h3>
                                <?php echo $data['email']; ?>
                                <br>
                                <h3>gender:</h3><?php echo $data['gender']; ?>
                                <br>
                                <h2>Contact Information</h2>
                                <h3>address:</h3><?php echo $data['address']; ?>
                                <br>
                                <h3>city:</h3><?php echo $data['city']; ?><br>
                                <h3>state:</h3><?php echo $data['state']; ?><br>
                                <h3>pincode:</h3><?php echo $data['pincode']; ?>
                                <br>
                                <h3>county:</h3><?php echo $data['country']; ?>
                                <br>
                                <h3>Contact:</h3><?php echo $data['phone']; ?>
                                <br><br>
                                <a data-toggle="pill" href="#editpage" 
                                   class="btn btn-primary" role="button">
                                    <span class="glyphicon glyphicon-pencil">
                                    </span> Edit Details
                                </a>
                            </p>
                        </div>
                        <div id="editpage" class="tab-pane fade">
                            <form action="dashboard.php" method="post">
                                <div class="form-group">
                                    <label for="name1">NAME:</label><br>
                                    <input type="text" class="form-control"
                                    id= "name1" name="name" 
                                    placeholder="First Name*" 
                                    value="<?php if(isset($_POST['name']))
                                                     echo $_POST['name']; ?>">
                                    <?php if(isset($errors['name1']))
                                             echo '<span class=
                                                "label label-danger">'
                                            .$errors['name1'].'</span>';
                                             if(isset($errors['name2']))
                                              echo '<span class=
                                          "label label-danger">'
                                            .$errors['name2'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="mname" placeholder="Middle Name"
                                        value="<?php if(isset($_POST['mname']))
                                             echo $_POST['mname']; ?>">
                                        <?php if(isset($errors['mname1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['mname1'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="lname" placeholder="Last Name*"
                                        value="<?php if(isset($_POST['lname']))
                                                    echo $_POST['lname']; ?>">
                                        <?php  if(isset($errors['lname1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['lname1'].'</span>';
                                        if(isset($errors['lname2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['lname2'].'</span>';?>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="gender"
                                        <?php if (isset($_POST['gender'])
                                                && $_POST['gender']=="1")
                                                echo "checked";?> value="1">
                                                    Male</label>
                                    &nbsp;
                                    <label><input type="radio" name="gender"
                                        <?php if (isset($_POST['gender'])
                                                && $_POST['gender']=="2")
                                                echo "checked";?> value="2">
                                                    Female</label>
                                        <?php  if(isset($errors['gender']))
                                        echo '<span class="label label-danger">'
                                            .$errors['gender'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <label for="address1">ADDRESS:</label><br>
                                    <input type="text" class="form-control"
                                        id= "address1" name="address"
                                        placeholder="Address*"
                                        value="<?php
                                        if(isset($_POST['address']))
                                            echo $_POST['address']; ?>">
                                        <?php  if(isset($errors['address']))
                                        echo '<span class="label label-danger">'
                                            .$errors['address'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="city" placeholder="City*"
                                        value="<?php if(isset($_POST['city']))
                                                    echo $_POST['city']; ?>">
                                        <?php  if(isset($errors['city']))
                                        echo '<span class="label label-danger">'
                                        .$errors['city'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input list="states" class="form-control"
                                        name="state" placeholder="state*"
                                        value="<?php if(isset($_POST['state']))
                                                    echo $_POST['state']; ?>">
                                    <datalist id="states">
                                        <option value="Odisha">
                                        <option value="Westbengal">
                                        <option value="Goa">
                                        <option value="Maharashtra">
                                        <option value="Delhi">
                                        <option value="Rajasthan">
                                        <option value="Karnataka">
                                    </datalist>
                                    <?php  if(isset($errors['state']))
                                        echo '<span class="label label-danger">'
                                            .$errors['state'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="pcode" placeholder="Postal code*"
                                        value="<?php if(isset($_POST['pcode']))
                                                echo $_POST['pcode']; ?>">
                                        <?php  if(isset($errors['pcode1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['pcode1'].'</span>';
                                        if(isset($errors['pcode2']))
                                        echo '<span class="label label-danger">'
                                        .$errors['pcode2'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                    name="country" placeholder="Country*"
                                    value="<?php if(isset($_POST['country']))
                                                echo $_POST['country']; ?>">
                                    <?php  if(isset($errors['country']))
                                        echo '<span class="label label-danger">'
                                            .$errors['country'].'</span>';?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="phoneno" placeholder="Phone*"
                                    value="<?php if(isset($_POST['phoneno']))
                                                echo $_POST['phoneno']; ?>">
                                        <?php  if(isset($errors['phoneno1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['phoneno1'].'</span>';
                                        if(isset($errors['phoneno2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['phoneno2'].'</span>';?>
                                </div>
                                <br>
                                <button type="submit" name="submit"
                                        class="btn btn-success">Submit
                                </button>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
    </body>
</html>