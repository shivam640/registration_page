<?php
    session_start();
    if (isset($_SESSION['id']) && isset($_SESSION['loggedIn'])){
        header('Location: dashboard.php');
        exit();
    } 
    require_once('config.php');
    require_once('connection.php');
    require_once('constant.php');
    require_once('mail.php');

    if (isset($_POST['submit'])) {
        $errors = array();
        $is_error=0;
        function isempty ($feild) {
            global $is_error;
            if(empty($feild)) {
                $is_error=1;
                return 'This feild can not be empty';
            }
        }
        function password_format ($expression, $feild) {
            global $is_error;
            if(!preg_match($expression, $feild) && !empty($feild)) {
                $is_error=1;
                return 'Password must be 8 characters long, contain atleast 1
                    letter and 1 digit!';
            }
        }
        function name_format ($expression, $feild) {
            global $is_error;
            if(!preg_match($expression, $feild)) {
                $is_error=1;
                return 'Only letters allowed!';
            }
        }
        $errors['emailaddress1'] = isempty($_POST['emailaddress']);
        if (!filter_var($_POST['emailaddress'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['emailaddress'])
        ) {
            $is_error=1;
            $errors['emailaddress2']= 'Invalid email format';
        }
        $sql = $conn->query("select id from user where email='"
            .$_POST['emailaddress']."'");
        if ($sql->num_rows > 0) {
            $is_error=1;
            $errors['emailaddress3'] = 'Email already exists';
        }
        $errors['password1'] = isempty($_POST['password1']);
        $errors['password2'] = password_format(
            password_expression,
            $_POST['password1']
        );
        $errors['cpassword1'] = isempty($_POST['cpassword1']);
        $errors['cpassword2'] = password_format(
            password_expression,
            $_POST['cpassword1']
        );
        if(strcmp($_POST['password1'],$_POST['cpassword1'])
            && !empty($_POST['cpassword1'])
        ) {
            $is_error=1;
            $errors['notequal']='Password did\'nt match';
        }
        $errors['name1'] = isempty($_POST['name']);
        $errors['name2'] = name_format(name_expression, $_POST['name']);
        $errors['mname1'] = name_format(name_expression, $_POST['mname']);
        $errors['lname1'] = isempty($_POST['lname']);
        $errors['lname2'] = name_format(name_expression, $_POST['lname']);
        $errors['gender'] = isempty($_POST['gender']);
        $errors['address'] = isempty($_POST['address']);
        $errors['city'] = isempty($_POST['city']);
        $errors['state'] = isempty($_POST['state']);
        $errors['pcode1'] = isempty($_POST['pcode']);
        if (!preg_match(pcode_expression,$_POST['pcode']) &&
            !empty($_POST['pcode'])) {
            $is_error=1;
            $errors['pcode2']= 'Must be 6 digits only';
        }
        $errors['country'] = isempty($_POST['country']);
        $errors['phoneno1'] = isempty($_POST['phoneno']);
        if (!preg_match(phone_expression,$_POST['phoneno']) &&
            !empty($_POST['phoneno'])) {
            $is_error=1;
            $errors['phoneno2']= 'Must be 10 digits only';
        }
        $errors['iagree'] = isempty($_POST['iagree']);
        if ($is_error == 0) {

            $text
            = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for ($i = 0; $i < 20; $i++)
                $token .= $text[mt_rand(0,61)];

            $hashedpassword = Password_hash(
                $_POST['password1'],
                PASSWORD_BCRYPT
            );
            $sql = "INSERT INTO user (email, password, fname, mname, lname,
                gender, address, city, state, pincode ,country, phone,
                verification_code, is_email_confirmed) VALUES ('"
                .$_POST['emailaddress']."', '$hashedpassword', '".$_POST['name']
                ."', '" .$_POST['mname']."', '".$_POST['lname']."', "
                .$_POST['gender'].",'".$_POST['address']."', '".$_POST['city'].
                "', '".$_POST['state']."', ".$_POST['pcode'].", '"
                .$_POST['country']."', ".$_POST['phoneno'].",'$token', '0')";
            if ($conn->query($sql) === TRUE) {
                send_mail(
                    $_POST['emailaddress'],
                    $_POST['name'],
                    $token,
                    $mail_address,
                    $mail_password,
                    $mail_username
                );
            }
            else {
                echo "<div>Error: ".$sql."<br>".$conn->error."</div>";
            }
        }   
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>registration page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/signup.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My registration page</a>
                </div>
                <div class="collapse navbar-collapse" id="navcollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">
                                <span class="glyphicon glyphicon-log-in"></span>
                                    Log In
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page">
            <form action="signup.php" method="post">
                <div class="form-group">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 panel panel-default" id="left">
                                <div class="panel-body">
                                    <h3><b>Preffered User Information</b></h3>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li class="active">
                                            <a data-toggle="pill"
                                               href="#emailnpassword">
                                                    Email and Password
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="pill"
                                               href="#acc_info">
                                                    Account Information
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="pill" href="#add">
                                                Contact Details
                                            </a>
                                        </li>
                                        <li><a data-toggle="pill"
                                               href="#agreement">
                                                Agreement
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class=
                                    "col-md-6 tab-content panel panel-default"
                                 id="right">
                                <div id="emailnpassword"
                                     class="tab-pane fade in active">
                                    <div class="form-group">
                                    <label for="email">EMAIL:</label><br>
                                    <input type="text" class="form-control"
                                        id="email" name="emailaddress"
                                        placeholder="Email Address*"
                                        value="<?php
                                        if(isset($_POST['emailaddress']))
                                            echo $_POST['emailaddress']; ?>">
                                    <?php  if(isset($errors['emailaddress1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['emailaddress1'].'</span>';
                                        if(isset($errors['emailaddress2']))
                                        echo '<span class="label label-danger">'
                                        .$errors['emailaddress2'].'</span>';
                                        if(isset($errors['emailaddress3']))
                                        echo '<span class="label label-danger">'
                                        .$errors['emailaddress3'].'</span>';?>
                                    </div>
                                    <div class="form-group">
                                    <label for="password">PASSWORD:</label><br>
                                    <input type="password" class="form-control"
                                        id= "password" name="password1"
                                        placeholder= "Password*"
                                        value="<?php 
                                            if(isset($_POST['password1']))
                                                echo $_POST['password1']; ?>">
                                    <?php if(isset($errors['password1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['password1'].'</span>';
                                        if(isset($errors['password2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['password2'].'</span>';?>
                                    </div>
                                    <div class="form-group">
                                    <label for="cpassword">CONFIRM PASSWORD:
                                    </label><br>
                                    <input type="password" class="form-control"
                                        id= "cpassword" name="cpassword1"
                                        placeholder= "Confirm Password*"
                                        value="<?php
                                            if(isset($_POST['cpassword1']))
                                                echo $_POST['cpassword1']; ?>">
                                    <?php if(isset($errors['cpassword1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['cpassword1'].'</span>';
                                    if(isset($errors['cpassword2']))
                                        echo '<span class="label label-danger">'
                                        .$errors['cpassword2'].'</span>';
                                    if(isset($errors['notequal']))
                                        echo '<span class="label label-danger">'
                                        .$errors['notequal'].'</span>';?>
                                    </div>
                                    <a data-toggle="pill" href="#acc_info"
                                       class="btn btn-primary" role="button">
                                       Next<span class
                                       ="glyphicon glyphicon-chevron-right">
                                       </span>
                                   </a>
                                </div>
                                <div id="acc_info" class="tab-pane fade">
                                    <div class="form-group">
                                    <label for="name1">NAME:</label><br>
                                    <input type="text" class="form-control"
                                        id= "name1" name="name"
                                        placeholder="First Name*"
                                        value="<?php if(isset($_POST['name']))
                                                echo $_POST['name']; ?>">
                                    <?php if(isset($errors['name1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['name1'].'</span>';
                                    if(isset($errors['name2'])) 
                                        echo '<span class="label label-danger">'
                                        .$errors['name2'].'</span>';?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="mname" placeholder="Middle Name"
                                        value="<?php if(isset($_POST['mname']))
                                                echo $_POST['mname']; ?>">
                                    <?php if(isset($errors['mname1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['mname1'].'</span>';?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="lname" placeholder="Last Name*"
                                        value="<?php if(isset($_POST['lname']))
                                                echo $_POST['lname']; ?>">
                                    <?php  if(isset($errors['lname1']))
                                        echo '<span class="label label-danger">'
                                        .$errors['lname1'].'</span>';
                                    if(isset($errors['lname2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['lname2'].'</span>';?>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender"
                                            <?php if (isset($_POST['gender'])
                                            && $_POST['gender']=="1")
                                                echo "checked";
                                            ?> 
                                                value="1">Male
                                            </label>
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="gender"
                                                <?php 
                                                    if (isset($_POST['gender'])
                                                    && $_POST['gender']=="2")
                                                    echo "checked";
                                                ?> 
                                                    value="2">Female
                                        </label>
                                        <?php  if(isset($errors['gender']))
                                        echo '<span class="label label-danger">'
                                            .$errors['gender'].'</span>';
                                        ?>
                                    </div>
                                    <a data-toggle="pill" href="#emailnpassword"
                                        class="btn btn-primary" role="button">
                                        <span class=
                                            "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <a data-toggle="pill" href="#add"
                                        class="btn btn-primary" role="button">
                                        Next<span class=
                                        "glyphicon glyphicon-chevron-right">
                                        </span>
                                    </a>
                                </div>
                                <div id="add" class="tab-pane fade">
                                    <div class="form-group">
                                    <label for="address1">ADDRESS:</label><br>
                                    <input type="text" class="form-control"
                                        id= "address1" name="address"
                                        placeholder="Address*"
                                        value="<?php
                                            if(isset($_POST['address']))
                                                    echo $_POST['address']; ?>">
                                    <?php  if(isset($errors['address']))
                                        echo '<span class="label label-danger">'
                                            .$errors['address'].'</span>';
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="city" placeholder="City*"
                                        value="<?php if(isset($_POST['city']))
                                                    echo $_POST['city']; ?>">
                                    <?php  if(isset($errors['city']))
                                        echo '<span class="label label-danger">'
                                                .$errors['city'].'</span>';
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input list="states" class="form-control"
                                        name="state" placeholder="state*"
                                        value="<?php if(isset($_POST['state']))
                                                echo $_POST['state']; ?>">
                                    <datalist id="states">
                                        <option value="Odisha">
                                        <option value="Westbengal">
                                        <option value="Goa">
                                        <option value="Maharashtra">
                                        <option value="Delhi">
                                        <option value="Rajasthan">
                                        <option value="Karnataka">
                                    </datalist>
                                    <?php  if(isset($errors['state']))
                                        echo '<span class="label label-danger">'
                                                .$errors['state'].'</span>'
                                    ;?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="pcode" placeholder="Postal code*"
                                        value="<?php if(isset($_POST['pcode']))
                                                    echo $_POST['pcode']; ?>">
                                    <?php  if(isset($errors['pcode1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['pcode1'].'</span>';
                                    if(isset($errors['pcode2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['pcode2'].'</span>';
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="country" placeholder="Country*"
                                        value="<?php
                                            if(isset($_POST['country']))
                                                    echo $_POST['country']; ?>">
                                    <?php  if(isset($errors['country']))
                                        echo '<span class="label label-danger">'
                                            .$errors['country'].'</span>';
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="phoneno" placeholder="Phone*"
                                        value="<?php
                                        if(isset($_POST['phoneno']))
                                            echo $_POST['phoneno']; ?>">
                                    <?php  if(isset($errors['phoneno1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['phoneno1'].'</span>';
                                        if(isset($errors['phoneno2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['phoneno2'].'</span>';
                                    ?>
                                    </div>
                                
                                    <a data-toggle="pill" href="#acc_info"
                                        class="btn btn-primary" role="button">
                                        <span class=
                                        "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <a data-toggle="pill" href="#agreement"
                                        class="btn btn-primary" role="button">
                                        Next<span class=
                                            "glyphicon glyphicon-chevron-right">
                                            </span>
                                    </a>
                                </div>
                                <div id="agreement" class="tab-pane fade">
                                    <p>Membership to this portal is public. once
                                     your account information has been submitted
                                    , you will be immediatly granted access to
                                    the portal environment. Your information
                                    is for verification purpose and will not
                                    be shared by our platform. Be sure that
                                    above mentioned details are true as per your
                                     knowledge and the company will not be
                                    responsible for any kind of issues. </p>
                                    <input type="checkbox"
                                    <?php if (isset($_POST['iagree']))
                                        echo "checked";
                                    ?> name="iagree"> I agree
                                    <?php  if(isset($errors['iagree']))
                                        echo '<span class="label label-danger">'
                                            .$errors['iagree'].'</span>';?>
                                    <br><br>
                                    <a data-toggle="pill" href="#add" 
                                        class="btn btn-primary" role="button">
                                        <span class=
                                        "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <button type="submit" name="submit"
                                        class="btn btn-success">Create account
                                    </button>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous">
        </script>
    </body>
</html>