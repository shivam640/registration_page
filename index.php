<?php
    session_start();
    if (isset($_SESSION['id']) && isset($_SESSION['loggedIn'])){
        header('Location: dashboard.php');
        exit();
    } 
    require_once('connection.php');
    $errors = array();
    if (isset($_POST['login'])) {
        $email = $conn->real_escape_string($_POST['emailaddress']);
        $password = $conn->real_escape_string($_POST['password']);
        if ($email == "" || $password == "") {
            $errors['error1'] = 'check yours inputs';
        } else {
            $sql = $conn-> query ("select id, password, is_email_confirmed from
                        user where email='".$email."'");
            if ($sql->num_rows > 0) {
                $data = $sql->fetch_array();
                if (password_verify($password, $data['password'])) {
                    if ($data['is_email_confirmed'] == 0) {
                        $errors['error2'] = 'Please verify your email';
                    } else {
                        $_SESSION['id'] = $data['id'];
                        $_SESSION['loggedIn'] = 1;
                        header('Location: dashboard.php');
                        exit();
                    }
                } else
                    $errors['error1'] = 'check yours inputs';
            } else
                $errors['error1'] = 'check yours inputs';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>login page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My page</a>
                </div>
                <div class="collapse navbar-collapse" id="navcollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="signup.php">
                                <span class="glyphicon glyphicon-log-in">
                                </span> Sign Up
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <form action="index.php" method="post">
            <div id="page">
                <div class="container">
                    <div class="row">
                        <div class=
                                "col-md-5 col-md-offset-3 panel panel-default">
                            <div class="panel-body text-center" id="text">
                                <h2> log in to my page</h2>
                                <input type="text" id="email"
                                    class="form-control"
                                    placeholder="email address"
                                    name="emailaddress"
                                    value="<?php
                                        if(isset($_POST['emailaddress']))
                                            echo $_POST['emailaddress']; ?>">
                                <br>
                                <input type= "password" id= "password"
                                    class="form-control" placeholder= "Password"
                                    name="password"
                                    value="<?php if(isset($_POST['password']))
                                                echo $_POST['password']; ?>">
                                <br>
                                <button type="submit" name="login"
                                    class="btn btn-primary form-control">
                                    <span class="glyphicon glyphicon-log-in">
                                    </span> Log In
                                </button>
                                <br>
                                <a href="forgetpassword.php">Forgotten account?
                                </a>
                                &nbsp;&nbsp;
                                <a href="signup.php">Sign up for my page</a>
                                <br>
                                <?php  if(isset($errors['error1']))
                                    echo '<span class="label label-danger">'
                                            .$errors['error1'].'</span>';
                                    if(isset($errors['error2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['error2'].'</span>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
    </body>
</html>