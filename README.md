# Loging And Logout Page
A web application that provide user to register, login, change password and change user information.

# Installation
## Pre-requisite
* Apache 2.4
* Mysql 5.7
+ PHP 7.0 with the following extensions installed
    * php7.0-xml 
    * php7.0-zip 
    * mcrypt 
    * php7.0-mcrypt 
    * php7.0-mbstring
    * php7.0-curl

## Installation steps
## Apache 2.4
```
sudo apt-get update
sudo apt-get install apache2 apache2-bin
```

## MYSQL 5.7
```
wget http://dev.mysql.com/get/mysql-apt-config_0.8.3-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.3-1_all.deb
sudo apt-get update
sudo apt-get install mysql-server
mysqld --initialize
```

## PHP 7.0
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0 php7.0-mysql
```

Search packages by running
```
apt-cache search php7.0
```
and install respective package by running
```
sudo apt-get install package-name
```
```
sudo apt-get install php7.0-xml php7.0-zip mcrypt php7.0-mcrypt php7.0-mbstring php7.0 curl

sudo phpenmod mcrypt
sudo phpenmod mbstring
```

## Enable mod rewrite
```
sudo a2enmod rewrite
sudo service apache2 restart
```

## GIT
```
sudo apt-get install git
```

## Composer
```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

## Running
* Create a virtual host configuration file with **.conf** extension.  
Example: **comptized.dev.conf**
```
sudo nano /etc/apache2/sites-available/[virtual_host_file_name]
```
* Add the following configuration:
```
<VirtualHost *:80>
    ServerAdmin [admin@domain]
    ServerName [domain_name]
    ServerAlias [domain_name]
    DocumentRoot [path_to_project's_public_directory]
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory [path_to_project's_public_directory]>
        Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>
</VirtualHost>
```
**Please replace the placeholders with appropriate values.**

* Run the following command to Enable the virtual host
```
sudo a2ensite [virtual_host_file_name]
```
* Restart Apache
```
sudo service apache2 restart
```
* Open Local Hosts file
```
sudo nano /etc/hosts
```
* Add the following to the Local Hosts file
```
127.0.0.1   [domain_name]
```
* Now open a browser, visit your domain and you should see the home page.

## Documents
Coding guidelines and other documents can be found in the "coding_guidelines" directory.