<?php
    require_once('connection.php');
    require_once('mail1.php');
    require_once('config.php');
    if (isset($_POST['submit'])) {
        $sql = $conn->query("select fname from user where email='"
               .$_POST['email']."' && is_email_confirmed=1");
        if ($sql->num_rows > 0) {
            $data = $sql->fetch_array();
            $text
            = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for ($i = 0; $i < 10; $i++)
                $token .= $text[mt_rand(0,61)];
            $conn->query("update user set is_email_confirmed=0,
                verification_code='".$token."' where email='"
                .$_POST['email']."'");
            send_mail(
                $_POST['email'],
                $data['fname'],
                $token,
                $mail_address,
                $mail_password,
                $mail_username
            );
        } else
            echo 'email does not exist';
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>forgetpassword</title>
        <link rel="stylesheet" 
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
            crossorigin="anonymous">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My page</a>
                </div>
                <div class="collapse navbar-collapse" id="navcollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="signup.php">
                                <span class="glyphicon glyphicon-log-in"></span>
                                Sign Up
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <form action="forgetpassword.php" method="post">
            <div id="page">
                <div class="container">
                    <div class="row">
                        <div class=
                                "col-md-5 col-md-offset-3 panel panel-default">
                            <div class="panel-body" id="text">
                                <h2> Find your account</h2>
                                <p>
                                    please enter your email address to
                                    search for your account.
                                </p>
                                <input type="text" name="email"
                                    class="form-control" 
                                    placeholder="email address"
                                    value="<?php if(isset($_POST['email']))
                                        echo $_POST['email']; ?>">
                                <br>
                                <button type="submit" name="submit"
                                        class="btn btn-primary">Search
                                </button>
                                <a href="index.php" class="btn btn-primary"
                                    role="button">Cancel
                                </a>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
    </body>
</html>