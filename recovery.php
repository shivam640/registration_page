<?php
    require_once('constant.php');
    require_once('connection.php');
    if (isset($_POST['submit'])) {
        $errors = array();
        $is_error=0;
        function isempty ($feild) {
            global $is_error;
            if(empty($feild)) {
                $is_error=1;
                return 'This feild can not be empty';
            }
        }
        function password_format ($expression, $feild) {
            global $is_error;
            if(!preg_match($expression, $feild) && !empty($feild)) {
                $is_error=1;
                return 'Password must be 8 characters long, 
                        contain atleast 1 letter and 1 digit!';
            }
        }
        $errors['password1'] = isempty($_POST['password']);
        $errors['password2'] = password_format(
                                    password_expression,
                                    $_POST['password']
                                );
        if ($is_error == 0) {
            session_start();
            $hashedpassword = Password_hash(
                                    $_POST['password'],
                                    PASSWORD_BCRYPT
                                );
            $sql = $conn->query("update user set password='"
                        .$hashedpassword."', is_email_confirmed=1, 
                        verification_code='' where id=".$_SESSION['id']);
            session_unset();
            session_destroy();
            header('Location: index.php');
            exit();
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>login page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My page</a>
                </div>
            </div>
        </nav>
        <form action="recovery.php" method="post">
            <div id="page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-3 panel panel-default">
                            <div class="panel-body" id="text">
                                <h2> Choose a new password</h2>
                                <p>
                                    A strong password is combination of letters 
                                    and digits and should be atleast 8 
                                    characters long!
                                </p>
                                <input type="password" class="form-control"
                                    placeholder="New password" name="password"
                                    value="<?php if(isset($_POST['password']))
                                        echo $_POST['password']; ?>">
                                    <?php if(isset($errors['password1']))
                                        echo '<span class="label label-danger">'
                                            .$errors['password1'].'</span>';
                                        if(isset($errors['password2']))
                                        echo '<span class="label label-danger">'
                                            .$errors['password2'].'</span>';?>
                                <br><br>
                                <button type="submit" name="submit"
                                        class="btn btn-primary">Submit
                                </button>
                                <a href="index.php" class="btn btn-primary"
                                   role="button">Skip
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
    </body>
</html>