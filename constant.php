<?php
    define('password_expression','/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/');
    define('name_expression','/^[a-zA-Z]*$/');
    define('pcode_expression','/^[0-9]{6}$/');
    define('phone_expression','/^[0-9]{10}$/');
?>
