<?php
    use PHPMailer\PHPMailer\PHPMailer;

    use PHPMailer\PHPMailer\Exception;

    require 'vendor/autoload.php';
    function send_mail(
                $to,
                $name,
                $token,
                $mail_address,
                $mail_password,
                $mail_username
            ) {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = $mail_address;
            $mail->Password = $mail_password;
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->setFrom($mail_address, $mail_username);
            $mail->addAddress($to, $name);
            $body = "<p><strong>Hi!</strong> <br><br>We received a request to
                    reset your My Page password.please click on the link below
                    to change your passford:<br><br><a href=
                    'http://localhost/registration_page/confirm1.php?email="
                    .$to."&token=$token'>Click Here</a></p>";
            $mail->isHTML(true);
            $mail->Subject = 'My Page';
            $mail->Body    = $body;
            $mail->AltBody = strip_tags($body);
            $mail->send();
            header('Location: success.php');
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }    
?>
